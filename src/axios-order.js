import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://react-burger-a9c7f.firebaseio.com'
});

export default instance;