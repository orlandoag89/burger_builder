import React, { Component } from 'react';
import Auxiliary from './../Auxiliary/Auxiliary';
import Modal from './../../components/UI/Modal/Modal';

const withErrorHandler = (WrapperedComponent, axios) => {
  return class extends Component {
    state = {
      error: null
    }

    componentWillMount() {
      this.reqInterceptor = axios.interceptors.request.use(req => {
        this.setState({ error: null });
        return req;
      });
      this.resInterceptor = axios.interceptors.response.use(res => res, error => {
        this.setState({ error: error });
      });
    }

    componentWillUnmount() {
      axios.interceptors.request.eject(this.reqInterceptor);
      axios.interceptors.response.eject(this.resInterceptor);
    }

    errorConfirmedHandler = () => {
      this.setState({ error: null });
    }

    render() {
      return (
        <Auxiliary>
          <Modal
            cancel={this.errorConfirmedHandler}
            show={this.state.error}>
            {this.state.error ? this.state.error.message : null}
          </Modal>
          <WrapperedComponent {...this.props} />
        </Auxiliary>
      );
    }
  }
}

export default withErrorHandler;