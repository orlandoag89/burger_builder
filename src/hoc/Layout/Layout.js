import React, { Component } from 'react';
import Auxiliary from '../Auxiliary/Auxiliary';
import Toolbar from '../../components/UI/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/UI/Navigation/SideDrawer/SideDrawer';
import './Layout.css'

class Layout extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showingSideDrawer: false
    }
  }

  sideDrawerClose = () => {
    this.setState({ showingSideDrawer: false })
  }

  toggleClicHandler = () => {
    this.setState(prevState => {
      return { showingSideDrawer: !prevState.showingSideDrawer }
    })
  }

  render() {
    return (
      <Auxiliary>
        <Toolbar drawerToggleClick={this.toggleClicHandler} />
        <SideDrawer
          close={this.sideDrawerClose}
          showing={this.state.showingSideDrawer} />
        <main className="Content">
          {this.props.children}
        </main>
      </Auxiliary>
    );
  }
}

export default Layout;