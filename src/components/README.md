Los componentes son componentes tontos, es decir, no manejan estados

*
  --save-prop-types -> Valida los tipos de datos

  BurgerIngredient.propTypes = {
    type: PropTypes.string.isRequired
  }

*
  prevState en el setState, es el estado anterior

  shouldComponentUpdate(nextProps, nextState) Es un método de los componentes clase para que el componente se renderice
    Es importante que se comparen las propiedades con nextproperties para que se renderice correctamente
    return false; //Nunca se renderizará