import React from 'react';
import BuildControl from './BuildControl/BuildControl';
import './BuildControls.css';

const controls = [
  { label: 'Salad', type: 'salad' },
  { label: 'Meat', type: 'meat' },
  { label: 'Cheese', type: 'cheese' },
  { label: 'Bacon', type: 'bacon' }
];

const buildControls = props => (
  <div className="BuildControls">
    <p><strong>Total price {props.totalPrice.toFixed(2)}</strong></p>
    {
      controls.map(el => (
        <BuildControl
          key={el.type}
          label={el.label}
          add={() => props.addIngredient(el.type)}
          remove={() => props.removeIngredient(el.type)}
          disabled={props.disabledInf[el.type]}
        />
      ))
    }
    <button
      className="OrderButton"
      disabled={props.isPurchasable}
      onClick={props.purchasing}
    >ORDER NOW</button>
  </div>
);

export default buildControls;