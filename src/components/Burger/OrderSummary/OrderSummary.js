import React, { Component } from 'react';
import Auxiliary from './../../../hoc/Auxiliary/Auxiliary';
import Button from './../../UI/Button/Button';

class OrderSummary extends Component {

  componentWillUpdate() {
    console.log('[ORD_SUMM] Component updated!');
  }

  render() {
    const ingredients = Object.keys(this.props.ingredients).map(key => (
      <li key={key} style={{ padding: '2px', marginLeft: '5px' }}>
        <span style={{ textTransform: 'capitalize' }}>{key}</span>: {this.props.ingredients[key]}
      </li>
    ));

    return (
      <Auxiliary>
        <h3>Order summary</h3>
        <p>A delicius burger with the following ingredients:</p>
        <u style={{ listStyle: 'none', textDecoration: 'none' }}>
          {ingredients}
        </u>
        <p><strong>Total price: {this.props.totalPrice.toFixed(2)}</strong></p>
        <p>Continue?</p>
        <Button btnType="Success" clicked={this.props.continue}>CONTINUE</Button>
        <Button btnType="Danger" clicked={this.props.cancel}>CANCEL</Button>
      </Auxiliary>
    );
  }
}

export default OrderSummary;