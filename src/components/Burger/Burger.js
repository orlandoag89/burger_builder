import React from 'react';

import BurgerIngredient from './BurgerIngredient/BurgerIngredient';
import './Burger.css';

const burger = props => {

  console.log('[BURG] props', props);

  let transformedIngredients = Object.keys(props.ingredients)
    .map(igKey => {
      // if (igKey === 'totalPrice') {
      //   return;
      // }
      return [...Array(props.ingredients[igKey])].map((_, index) => {
        return <BurgerIngredient key={index + igKey} type={igKey} />
      })
    }).reduce((prev, current) => {
      return prev.concat(current);
    }, []);

  if (transformedIngredients.length === 0) {
    transformedIngredients = <p>Please start adding ingredients!</p>;
  }

  return (
    <div className="Burger">
      <BurgerIngredient type="breadTop" />
      {transformedIngredients}
      <BurgerIngredient type="breadBottom" />
    </div>
  );
}

export default burger;