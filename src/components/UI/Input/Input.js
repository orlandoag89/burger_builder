import React from 'react';

import './Input.css'

const input = props => {
  let inputElement = null;
  const inputClasses = ['InputElement'];

  if (props.isValid && props.shoulValidate && props.touched) {
    inputClasses.push('Invalid')
  }

  switch (props.inputtype) {
    case 'input':
      inputElement = <input
        className={inputClasses.join(' ')}
        {...props.configElement}
        value={props.value}
        onChange={props.change} />;
      break;
    case 'textArea':
      inputElement = <textarea
        {...props.configElement}
        value={props.value}
        onChange={props.change} />;
      break;
    case 'select':
      inputElement = (<select
        className={inputClasses.join(' ')}
        value={props.value}
        onChange={props.change}>
        {
          props.configElement.options.map(e => (
            <option key={e.value} value={e.value}>{e.displayValue}</option>
          ))
        }
      </select>);
      break;
    default:
      inputElement = <input
        {...props.configElement}
        value={props.value} />;
  }

  return (
    <div className="Input">
      <label className="Label">{props.label}</label>
      {inputElement}
    </div>
  )
};

export default input;