import React from 'react';
import Logo from './../../../Logo/Logo';
import NavigationItems from './../NavigationItems/NavigationItems';
import DrawerToogle from './../SideDrawer/DrawerToogle/DrawerToogle';
import './Toolbar.css';

const toolbar = props => (
  <header className="Toolbar">
    <DrawerToogle clicked={props.drawerToggleClick} />
    <div className="LogoToolbar">
      <Logo />
    </div>
    <nav className="DesktopOnly">
      <NavigationItems />
    </nav>
  </header>
);

export default toolbar;