import React from 'react';
import Logo from './../../../Logo/Logo';
import NavigationItems from './../NavigationItems/NavigationItems';
import Backdrop from './../../Backdrop/Backdrop';
import Auxiliary from './../../../../hoc/Auxiliary/Auxiliary';
import './SideDrawer.css';

const sideDrawer = props => {
  let classesInSideDrawer = ['SideDrawer', 'Close'];

  if (props.showing) {
    classesInSideDrawer = ['SideDrawer', 'Open'];
  }

  return (
    <Auxiliary>
      <Backdrop show={props.showing} close={props.close} />
      <div className={classesInSideDrawer.join(' ')}>
        <div className="LogoSideDrawer">
          <Logo />
        </div>
        <nav>
          <NavigationItems />
        </nav>
      </div>
    </Auxiliary>
  );
};

export default sideDrawer;