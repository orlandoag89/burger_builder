import React from 'react';
import './Button.css';

const button = props => (
  <button
    className={'Button'.concat(' '.concat(props.btnType))}
    onClick={props.clicked}
    disabled={props.isDisabled}
  >{props.children}</button>
);

export default button;