import React, { Component } from 'react';
import Auxiliary from './../../../hoc/Auxiliary/Auxiliary';
import Backdrop from './../Backdrop/Backdrop';
import './Modal.css';

class Modal extends Component {

  shouldComponentUpdate(nextProps, nextState) {
    let updated = nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    console.log('[MOD] next props', nextProps);
    console.log('[MOD] this.props', this.props);
    return updated;
  }

  componentWillUpdate() {
    console.log('[MODAL] component will updated!');
  }
  render() {
    return (
      <Auxiliary>
        <Backdrop show={this.props.show} close={this.props.cancel} />
        <div className="Modal"
          style={{
            transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
            opacity: this.props.show ? '1' : '0'
          }}>
          {this.props.children}
        </div>
      </Auxiliary>
    );
  }
}

export default Modal;