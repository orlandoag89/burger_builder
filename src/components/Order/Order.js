import React from 'react';
import './Order.css';

const order = props => {
  const innerIngredients = [];

  for (const key in props.ingredients) {
    innerIngredients.push(<li key={key}
      style={{
        textTransform: 'capitalize',
        display: 'inline-block',
        margin: '0 8px',
        border: '1px solid #ccc',
        padding: '5px'
      }}>{key} ({props.ingredients[key]})</li>);
  }

  return (
    <div className="Order">
      <p>Ingredients: </p><ul style={{ listStyle: 'none' }}>{innerIngredients}</ul>
      <p>Price: <strong>USD {props.price}</strong></p>
    </div >
  );
}

export default order;