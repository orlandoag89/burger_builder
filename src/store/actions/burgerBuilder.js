import * as actionTypes from './actionType';
import axios from './../../axios-order';

export const addIngredient = ingredientName => {
  return {
    type: actionTypes.ADD_INGREDIENTS,
    ingredientName: ingredientName
  }
};

export const removeIngredient = ingredientName => {
  return {
    type: actionTypes.REMOVE_INGREDIENTS,
    ingredientName: ingredientName
  }
};

export const setIngredients = ingredients => {
  return {
    type: actionTypes.SET_INGREDIENTS,
    ingredients: ingredients
  }
}

export const fetchIngredientsFailed = () => {
  return {
    type: actionTypes.FETCH_INGREDIENTS_FAILED
  }
}

export const initiIngredients = () => {
  return dispatch => {
    axios.get('/ingredients.json')
      .then(response => {
        dispatch(setIngredients(response.data));
      })
      .catch(error => {
        dispatch(fetchIngredientsFailed())
      })
  }
}