import * as actionType from '../actions/actionType';
import { updateObject } from './../utility';

const initialState = {
  ingredients: null,
  totalPrice: 4,
  hasError: false
}

const INGREDIENTS_PRICES = {
  meat: 2.5,
  cheese: 1,
  salad: .75,
  bacon: 2
}

const addIngredients = (state, action) => {
  const updateIngredients = { [action.ingredientName]: state.ingredients[action.ingredientName] + 1 };
  const updatedIngredients = updateObject(state.ingredients, updateIngredients);
  const updatedState = {
    ingredients: updatedIngredients,
    totalPrice: state.totalPrice + INGREDIENTS_PRICES[action.ingredientName]
  }
  return updateObject(state, updatedState);
}

const removeIngredient = (state, action) => {
  const ingredientCant = state.ingredients[action.ingredientName];
  if (ingredientCant < 0) {
    return;
  }
  const updateIngr = { [action.ingredientName]: state.ingredients[action.ingredientName] - 1 };
  const updatedIngr = updateObject(state.ingredients, updateIngr);
  const updatedSt = {
    ingredients: updatedIngr,
    totalPrice: state.totalPrice - INGREDIENTS_PRICES[action.ingredientName]
  }
  return updateObject(state, updatedSt);
}

const setIngredients = (state, action) => {
  return updateObject(state, {
    ingredients: {
      salad: action.ingredients.salad,
      bacon: action.ingredients.bacon,
      cheese: action.ingredients.cheese,
      meat: action.ingredients.meat
    },
    totalPrice: 4,
    hasError: false
  });
}

const fetchIngredientsFailed = (state, action) => {
  return updateObject(state, { hasError: true });
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.ADD_INGREDIENTS:
      /**
       return {
        ...state,
        ingredients: {
          ...state.ingredients,
          [action.ingredientName]: state.ingredients[action.ingredientName] + 1,
        },
        totalPrice: state.totalPrice + INGREDIENTS_PRICES[action.ingredientName]
      }
       */
      return addIngredients(state, action);
    case actionType.REMOVE_INGREDIENTS:
      return removeIngredient(state, action);
    case actionType.SET_INGREDIENTS:
      return setIngredients(state, action);
    case actionType.FETCH_INGREDIENTS_FAILED:
      return fetchIngredientsFailed(state, action);
    default:
      return state;
  }
}

export default reducer;