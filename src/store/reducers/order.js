import * as actionsTypes from './../actions/actionType';
import { updateObject } from './../utility';

const initialState = {
  orders: [],
  loading: false,
  purchased: false
};

const purchaseInit = (state, action) => {
  return updateObject(state, { purchased: false });
}

const purchaseBurgerstart = (state, action) => {
  return updateObject(state, { loading: true });
}

const purchaseBurgerSuccess = (state, action) => {
  const newOrder = updateObject(action.orderData, { id: action.orderId, });
  return updateObject(state, {
    purchased: true,
    loading: false,
    orders: state.orders.concat(newOrder)
  });
}

const purchaseBurgerFail = (state, action) => {
  return updateObject(state, { loading: false });
}

const fetchOrdersStart = (state, action) => {
  return updateObject(state, { loading: true });
}

const fetchOrdersSuccess = (state, action) => {
  return updateObject(state, { loading: false, orders: action.orders });
}

const fetchIngredientsFailed = (state, action) => {
  return updateObject(state, { loading: false });
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionsTypes.PURCHASE_INIT:
      return purchaseInit(state, action);
    case actionsTypes.PURCHASE_BURGER_START:
      return purchaseBurgerstart(state, action);
    case actionsTypes.PURCHASE_BURGER_SUCCESS:
      return purchaseBurgerSuccess(state, action);
    case actionsTypes.PURCHASE_BURGER_FAIL:
      return purchaseBurgerFail(state, action);
    case actionsTypes.FETCH_ORDERS_START:
      return fetchOrdersStart(state, action);
    case actionsTypes.FETCH_ORDERS_SUCCESS:
      return fetchOrdersSuccess(state, action);
    case actionsTypes.FETCH_INGREDIENTS_FAILED:
      return fetchIngredientsFailed(state, action);
    default:
      return state;
  }
}

export default reducer;
