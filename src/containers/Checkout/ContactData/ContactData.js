import React, { Component } from 'react';
import axios from './../../../axios-order';

import Spinner from './../../../components/UI/Spinner/Spinner';
import Button from './../../../components/UI/Button/Button';
import Input from './../../../components/UI/Input/Input';

import { connect } from 'react-redux';

import withErrorHandler from './../../../hoc/WithErrorHandler/WithErrorHandler';
import * as actionsTypes from './../../../store/actions/index'

import './ContactData.css';

class ContactData extends Component {

  state = {
    orderForm: {
      name: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your Name'
        },
        validationsRules: {
          required: true
        },
        valid: false,
        touched: false,
        value: ''
      },
      street: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your Street'
        },
        validationsRules: {
          required: true
        },
        valid: false,
        touched: false,
        value: ''
      },
      zipCode: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your Zip Code'
        },
        validationsRules: {
          required: true,
          isNumeric: true,
          minLength: 5,
          maxLength: 5
        },
        valid: false,
        touched: false,
        value: ''
      },
      country: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your Country'
        },
        validationsRules: {
          required: true
        },
        valid: false,
        touched: false,
        value: ''
      },
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'Your Email'
        },
        validationsRules: {
          required: true,
          isEmail: true
        },
        valid: false,
        touched: false,
        value: ''
      },
      deliveryMethod: {
        elementType: 'select',
        elementConfig: {
          options: [
            { value: 'fastest', displayValue: 'Fastest' },
            { value: 'cheapest', displayValue: 'Cheapest' }
          ]
        },
        value: 'fastest',
        validation: {},
        valid: true
      }
    },
    isValidForm: false
  }

  orderHandler = evt => {
    evt.preventDefault();
    console.log('[CONTACT_DATA] ingredients', this.props.ingredients);

    const formData = {};
    for (let key in this.state.orderForm) {
      formData[key] = this.state.orderForm[key].value
    }

    const order = {
      ingredients: this.props.ingredients,
      price: this.props.price,
      contactData: formData
    }

    this.props.onOrderBurger(order);

  }

  checkValidation(value, rules) {
    let isValid = true;

    if (!rules) {
      return true;
    }

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }

    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    }

    if (rules.isNumeric) {
      const pattern = /^\d+$/;
      isValid = pattern.test(value) && isValid
    }

    return isValid;
  }

  changeHandler = (evt, identifier) => {
    const updateOrderForm = {
      ...this.state.orderForm
    }
    const updateElement = {
      ...updateOrderForm[identifier]
    }

    updateElement.value = evt.target.value;
    updateElement.valid = this.checkValidation(updateElement.value, updateElement.validationsRules);
    updateElement.touched = true;
    updateOrderForm[identifier] = updateElement;

    let formIsValid = true;
    for (const key in updateOrderForm) {
      formIsValid = updateOrderForm[key].valid && formIsValid;
    }

    console.log(`[CONT_DATA] ${identifier} [${formIsValid}] config: `, updateOrderForm);
    this.setState({ orderForm: updateOrderForm, isValidForm: formIsValid });
  }

  render() {
    const elementsForms = [];

    for (const key in this.state.orderForm) {
      elementsForms.push({
        id: key,
        config: this.state.orderForm[key]
      });
    }

    let form = (
      <form onSubmit={this.orderHandler}>
        {
          elementsForms.map(_e => (
            <Input
              key={_e.id}
              inputtype={_e.config.elementType}
              configElement={_e.config.elementConfig}
              value={_e.value}
              isValid={!_e.config.valid}
              shoulValidate={_e.config.validationsRules}
              touched={_e.config.touched}
              change={evt => this.changeHandler(evt, _e.id)} />
          ))
        }
        <Button btnType="Success" isDisabled={!this.state.isValidForm}>ORDER</Button>
      </form>
    );

    if (this.props.loading) {
      form = <Spinner />
    }
    return (
      <div className="ContactData">
        <h4>Enter your Contact data</h4>
        {form}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    ingredients: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.totalPrice,
    loading: state.order.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onOrderBurger: orderData => dispatch(actionsTypes.purchaseBurger(orderData))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(ContactData, axios));