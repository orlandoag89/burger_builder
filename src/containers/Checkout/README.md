const query = new URLSearchParams(this.props.location.search); //?bacon=1&cheese=1&meat=1&salad=1
for (let param of query.entries()) {
  ingredients[param[0]] = +param[1]; //bacon: 1 El signo más convierte a entero ese string
}


Permite enviar propiedades al componente
Las mismas propiedades del componente padre como el history se heredan a sus hijos
<Route
  path={this.props.match.path + '/contact-data'}
  render={(props) => (<ContactData ingredients={this.state.ingredients} {...props} />)} 
/>

Únicamente renderiza el componente
<Route path={this.props.match.path + '/contact-data'} component={ContactData} />

