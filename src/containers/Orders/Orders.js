import React, { Component } from 'react';

import Order from './../../components/Order/Order';
import Spinner from './../../components/UI/Spinner/Spinner';
import WithErrorHandler from './../../hoc/WithErrorHandler/WithErrorHandler';
import * as actions from './../../store/actions/index'

import axios from './../../axios-order';
import { connect } from 'react-redux';

class Orders extends Component {
  componentDidMount() {
    this.props.onFetchOrders();
  }

  render() {

    let conentOrders = <Spinner />

    if (!this.props.loading) {
      conentOrders = this.props.orders.map(_e => (
        <Order key={_e.id}
          ingredients={_e.ingredients}
          price={_e.price} />)
      );
    }

    return (
      <div>
        {conentOrders}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    orders: state.order.orders,
    loading: state.order.loading
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onFetchOrders: () => dispatch(actions.fetchOrders())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WithErrorHandler(Orders, axios));