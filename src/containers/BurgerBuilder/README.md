El método keys devuelve las llaves de un objeto json.

const tmp = {
  'Hola': 'mundo',
  'Bienvenidos': 'Al medievo'
}

Object.keys(tmp).map(e => {
  console.log(e); //Hola, Bienvenidos
});

Object.keys(tmp).map(e => {
  console.log(tmp[e], e); //Hola, mundo, Bienvenidos, Al medievo
});
