import React, { Component } from 'react';

import { connect } from 'react-redux'

import axios from './../../axios-order';

import Burger from './../../components/Burger/Burger';
import Auxiliary from './../../hoc/Auxiliary/Auxiliary';
import BuildControls from './../../components/Burger/BuildControls/BuildControls';
import Modal from './../../components/UI/Modal/Modal';
import OrederSummary from './../../components/Burger/OrderSummary/OrderSummary';
import Spinner from './../../components/UI/Spinner/Spinner';
import WithErrorHandler from './../../hoc/WithErrorHandler/WithErrorHandler';

import * as burgerBuilderActions from './../../store/actions/index'

class BurgerBuilder extends Component {

  constructor(props) {
    super(props);
    this.state = {
      purchasing: false
    }
  }

  componentDidMount() {
    this.props.onLoadIngredients();
  }

  purchasingHandler = () => {
    this.setState({ purchasing: true })
  }

  cancelHandler = () => {
    this.setState({ purchasing: false })
  }

  continueHandler = () => {
    this.props.onInitPurchase();
    this.props.history.push('/checkout');
  }

  purchasableHandler = innerIngredients => {

    const sum = Object.keys(innerIngredients).map(key => {
      console.log('[BUR_BUIL] innerIngredients ', innerIngredients[key]);
      return innerIngredients[key];
    }).reduce((prev, current) => {
      return prev + current
    }, 0);
    return sum > 0;
  }

  render() {
    const disabledInfo = {
      ...this.props.ingredients
    }

    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0;
    }

    console.log('[BURG_BUIL] disabledInfo', disabledInfo);

    let orderSummary = null;

    let burgerMenu = this.props.hasError ? <p>Ingredients cannot be loaded!</p> : <Spinner />;

    if (this.props.ingredients) {

      burgerMenu = (
        <Auxiliary>
          <Burger ingredients={this.props.ingredients} />
          <BuildControls
            totalPrice={this.props.price}
            addIngredient={this.props.onAddIngredient}
            removeIngredient={this.props.onRemoveIngredient}
            disabledInf={disabledInfo}
            isPurchasable={!this.purchasableHandler(this.props.ingredients)}
            purchasing={this.purchasingHandler}
          />
        </Auxiliary>
      );

      orderSummary = <OrederSummary
        totalPrice={this.props.price}
        continue={this.continueHandler}
        cancel={this.cancelHandler}
        ingredients={this.props.ingredients}
      />;
    }

    // if (this.state.loading) {
    //   orderSummary = <Spinner />
    // }

    return (
      <Auxiliary>
        <Modal show={this.state.purchasing} cancel={this.cancelHandler}>
          {orderSummary}
        </Modal>
        {burgerMenu}
      </Auxiliary >
    );
  }
}

const mapStateToProps = state => {
  return {
    ingredients: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.totalPrice,
    hasError: state.burgerBuilder.hasError
  }
}

const mapPropsToDispatch = dispatch => {
  return {
    onAddIngredient: ingredientName => dispatch(burgerBuilderActions.addIngredient(ingredientName)),
    onRemoveIngredient: ingredientName => dispatch(burgerBuilderActions.removeIngredient(ingredientName)),
    onLoadIngredients: () => dispatch(burgerBuilderActions.initiIngredients()),
    onInitPurchase: () => dispatch(burgerBuilderActions.purchaseInit())
  }
}

export default connect(mapStateToProps, mapPropsToDispatch)(WithErrorHandler(BurgerBuilder, axios));